import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './components/layout/Header';
import Todos from './components/todos/Todos';
import AddTodo from './components/todos/AddTodo';
import Expenses from './components/expenses/Expenses';
import AddExpense from './components/expenses/AddExpense';
import About from './components/pages/About';
import uuid from 'uuid';
import axios from 'axios';
import './App.css';

class App extends Component {
    state = {
        todos: [],
        expenses: [],
    };

    componentDidMount() {
        axios
            .get('https://jsonplaceholder.typicode.com/todos?_limit=10')
            .then(res => this.setState({ todos: res.data }));
    }

    // Toggle Complete
    markComplete = id => {
        this.setState({
            todos: this.state.todos.map(todo => {
                if (todo.id === id) {
                    todo.completed = !todo.completed;
                }
                return todo;
            }),
        });
    };

    // Delete Todo
    delTodo = id => {
        this.setState({
            todos: [...this.state.todos.filter(todo => todo.id !== id)],
        });
    };
    // Delete Expense
    delExpense = id => {
        this.setState({
            expenses: [
                ...this.state.expenses.filter(expense => expense.id !== id),
            ],
        });
    };

    // Add Todo
    addTodo = title => {
        const newTodo = {
            id: uuid.v4(),
            title,
            completed: false,
        };
        this.setState({ todos: [...this.state.todos, newTodo] });
    };
    // Add Expense
    addExpense = (title, value) => {
        const newExpense = {
            id: uuid.v4(),
            title,
            value,
        };
        this.setState({ expenses: [...this.state.expenses, newExpense] });
    };

    render() {
        return (
            <Router>
                <div className="App">
                    <div className="container">
                        <Header />
                        <Route
                            exact
                            path="/"
                            render={props => (
                                <React.Fragment>
                                    <AddExpense addExpense={this.addExpense} />
                                    <Expenses
                                        expenses={this.state.expenses}
                                        delExpense={this.delExpense}
                                    />
                                </React.Fragment>
                            )}
                        />
                        <Route
                            exact
                            path="/todo"
                            render={props => (
                                <React.Fragment>
                                    <AddTodo addTodo={this.addTodo} />
                                    <Todos
                                        todos={this.state.todos}
                                        markComplete={this.markComplete}
                                        delTodo={this.delTodo}
                                    />
                                </React.Fragment>
                            )}
                        />
                        <Route path="/about" component={About} />
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
