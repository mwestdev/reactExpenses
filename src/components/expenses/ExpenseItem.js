import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ExpenseItem extends Component {
    render() {
        const { id, title, value } = this.props.expense;
        return (
            <tr>
                <td>{title}</td>
                <td>{value}</td>
                <td>
                    <button
                        onClick={this.props.delExpense.bind(this, id)}
                        style={btnStyle}
                    >
                        x
                    </button>
                </td>
            </tr>
        );
    }
}

// PropTypes
ExpenseItem.propTypes = {
    expense: PropTypes.object.isRequired,
    delExpense: PropTypes.func.isRequired,
};

const btnStyle = {
    background: '#ff0000',
    color: '#fff',
    border: 'none',
    padding: '5px 10px',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right',
};

export default ExpenseItem;
