import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class AddExpense extends Component {
    state = {
        title: '',
        value: 0,
    };

    onSubmit = e => {
        e.preventDefault();
        this.props.addExpense(this.state.title, this.state.value);
        this.setState({ title: '', value: 0 });
    };

    onChange = e => this.setState({ [e.target.name]: e.target.value });

    render() {
        return (
            <form onSubmit={this.onSubmit} style={{ display: 'flex' }}>
                <input
                    type="text"
                    name="title"
                    placeholder="Add Expense ..."
                    style={{ flex: '10', padding: '5px' }}
                    value={this.state.title}
                    onChange={this.onChange}
                    required
                />
                <input
                    type="number"
                    name="value"
                    style={{ flex: '10', padding: '5px' }}
                    value={this.state.value}
                    onChange={this.onChange}
                />
                <input
                    type="submit"
                    value="Submit"
                    className="btn"
                    style={{ flex: '1' }}
                />
            </form>
        );
    }
}

// PropTypes
AddExpense.propTypes = {
    addExpense: PropTypes.func.isRequired,
};

export default AddExpense;
