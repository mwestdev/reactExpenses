import React, { Component } from 'react';
import ExpenseItem from './ExpenseItem';
import PropTypes from 'prop-types';

class Expenses extends Component {
    renderTableData() {
        return this.props.expenses.map(expense => (
            <ExpenseItem
                key={expense.id}
                expense={expense}
                delExpense={this.props.delExpense}
            />
        ));
    }

    render() {
        return (
            <table style={tableStyle}>
                <tbody>{this.renderTableData()}</tbody>
            </table>
        );
    }
}

// PropTypes
Expenses.propTypes = {
    expenses: PropTypes.array.isRequired,
    delExpense: PropTypes.func.isRequired,
};

const tableStyle = {
    width: '100%',
};

export default Expenses;
